﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lab1.Main
{
    public struct LabDescriptor
    {
        #region P1

        public static Type IBase = typeof(void);
        
        public static Type ISub1 = typeof(void);
        public static Type Impl1 = typeof(void);
        
        public static Type ISub2 = typeof(void);
        public static Type Impl2 = typeof(void);
        
        
        public static string baseMethod = "...";
        public static object[] baseMethodParams = new object[] { };

        public static string sub1Method = "...";
        public static object[] sub1MethodParams = new object[] { };

        public static string sub2Method = "...";
        public static object[] sub2MethodParams = new object[] { };

        #endregion

        #region P2

        public static string collectionFactoryMethod = "...";
        public static string collectionConsumerMethod = "...";

        #endregion

        #region P3

        public static Type IOther = typeof(void);
        public static Type Impl3 = typeof(void);

        public static string otherCommonMethod = "...";
        public static object[] otherCommonMethodParams = new object[] { };

        #endregion
    }
}
